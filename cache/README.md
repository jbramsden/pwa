# PWA Cache tutorial example

## Introduction
This is a very basic PWA example. All it does is display the word cache demo on a html page. However if there is no network, so it is offline the page will still display.

## Files
* main.go <- Very simple file webserver written in go. This is just to provide the files to the browser
* index.html <- HTML page which the browser will call.
* cachedemo.css <- Basic CSS file, which will be cached by the PWA app.
* service-worker.js <- Has the javascript code for PWA to handle the caching.
* manifest.json <- Has the icons that will be used and also the starting url. Plus background colours.
* icons folder <- Has 5 different icons for the PWA app.

## main.go
Not really using any of the great features of GO here. Just using net/http to provide files over http from the current directory and listening on port 8087
[`	
	fs := http.FileServer(http.Dir("./"))
	http.Handle("/", http.StripPrefix("/", fs))

	http.ListenAndServe(":8087", nil) 
`]

## index.html
Three keys areas that we are implementing here so that we can have PWA cache demo.
1. We are requesting a CSS file from the server
`<link rel="stylesheet" type="text/css" href="cachedemo.css" media="all">`
1. We are requesting a manifest file from the server
` <link rel="manifest" href="manifest.json">`
1. We check if serviceWorker is available and make a call to the server for the service-worker.js file.
`
	<script>
      if('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/service-worker.js')
          .then(function() {
                console.log('Service Worker Registered');
          });
      }
    </script>
`

 